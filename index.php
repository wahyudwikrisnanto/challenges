<?php
require_once 'App/Controllers/MessageController.php';
?>
<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
    <link rel="stylesheet" href="./Assets/style.css">
    <title>challange1</title>
</head>

<body>
    <div class="container py-4 my-4">
        <div class="card card-body border-0" style="margin-bottom: 50px;">
            <?php
            if (isset($_SESSION["errors"])) {
                $errors = $_SESSION["errors"]; ?>
                <div style="background-color: #ffd9d7; padding: 10px 10px; margin-bottom: 20px;">
                    <ul>
                        <?php foreach ($errors as $error) { ?>
                            <?php foreach ($error as $e) { ?>
                                <li class="text-danger"><?php echo $e ?></li>
                            <?php } ?>
                        <?php } ?>
                    </ul>
                </div>
            <?php } ?>
            <form action="web.php?action=store" method="POST">
                <div class="form-group">
                    <label for="title">Title</label>
                    <input type="text" placeholder="Write down your title here..." value="<?php echo isset($_SESSION['old']['title']) ? $_SESSION['old']['title'][0] : ''; ?>" class="form-control" name="title" id="title">
                </div>
                <div class="form-group">
                    <label for="body">Body</label>
                    <textarea name="body" placeholder="Write down your message here..." id="body" cols="30" rows="5" class="form-control"><?php echo isset($_SESSION['old']['body']) ? $_SESSION['old']['body'][0] : ''; ?></textarea>
                </div>
                <div class="d-flex justify-content-center">
                    <button class="btn btn-secondary w-100" type="submit">Submit</button>
                </div>
            </form>
        </div>
        <?php 
        $controller = new MessageController;
        $messages   = $controller->showMessage(); 
        ?>
        <?php if (!empty($messages['data'])) { ?>
            <?php
            foreach ($messages['data'] as $message) {
            ?>
                <div class="card mb-4 border-0" style="border-top: solid black 1px !important; border-radius: 0;">
                    <div class="card-body">
                        <div style="display: flex; flex-direction: row; justify-content: space-between;">
                            <h3><?php echo $message['title'] ?></h3>
                            <h4><strong>#<?php echo $messages['iteration']-- ?></strong></h4>
                        </div>
                        <p><?php echo $message['body'] ?></p>
                        <div class="d-flex justify-content-end">

                            <div><?php echo $message['created_at'] ?></div>
                        </div>
                    </div>
                </div>
            <?php } ?>
        <?php } else { ?>
            <p class="text-center mt-4"><small><strong>No data found!</strong></small></p>
        <?php } ?>
        <div style="display: flex; justify-content: flex-end;">
            <div class="pagination-wrapper">
                <?php print_r($messages['page']) ?>
            </div>
        </div>
    </div>

    <script>
        // var elementsEdit = document.getElementsByClassName("btnEdit");
        // var elementsDelete = document.getElementsByClassName("btnDelete");
        // var elementsPagination = document.getElementsByClassName("pagination-link");

        // console.log(elementsPagination);

        // var get_data = function() {
        //     var id = this.getAttribute('data-id');
        //     var xhr = new XMLHttpRequest();
        //     xhr.open('GET', 'web.php?action=get&id=' + id);
        //     xhr.send();
        //     xhr.onload = function() {
        //         $data = JSON.parse(xhr.responseText);
        //         document.getElementById('title').value = $data.title;
        //         document.getElementById('body').innerHTML = $data.body;
        //     }
        //     document.documentElement.scrollTop = 0;
        // };

        // var delete_data = function(e) {
        //     var id = this.getAttribute('data-id');
        //     var data = new FormData();
        //     data.append('id', id);
        //     e.preventDefault();
        //     var xhr = new XMLHttpRequest();
        //     xhr.open('POST', 'web.php?action=delete');
        //     xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
        //     xhr.send(data);
        //     xhr.onload = function() {}
        // }

        // var page_action = function(e) {
        //     e.preventDefault();
        //     var page = this.getAttribute('href').split('page=')[1];
        //     console.log(page);
        //     fetch_data(page);
        // }

        // function fetch_data(page) {
        //     var xhr = new XMLHttpRequest();
        //     xhr.open('GET', 'web.php?action=fetch_data&page='+page);
        //     xhr.send();
        //     xhr.onload = function() {

        //     }
        // }

        // for (var index = 0; index < elementsEdit.length; index++) {
        //     elementsEdit[index].addEventListener('click', get_data, false);
        // }

        // for (var index = 0; index < elementsDelete.length; index++) {
        //     elementsDelete[index].addEventListener('click', delete_data, false);
        // }

        // for (var index = 0; index < elementsPagination.length; index++) {
        //     elementsPagination[index].addEventListener('click', page_action, false);
        // }
    </script>


    <!-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous"></script>
</body>
</html>
<?php session_unset(); ?>