<?php

class Connection {

    const HOST = 'localhost';
    const USERNAME = 'root';
    const PASSWORD = '';
    const DB_NAME = 'challenge30';

    public $connection = '';

    function __construct()
    {
        $this->connection = mysqli_connect(self::HOST, self::USERNAME, self::PASSWORD, self::DB_NAME);
        if(mysqli_connect_error()) {
            echo "Database connection error : " . mysqli_connect_error();
        }
    }
}

?>