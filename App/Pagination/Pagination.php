<?php

class Pagination
{
    protected $result;
    protected $iteration;

    function paginate($data, $perPage, $currentPage, $pageToShow)
    {
        $countData       = count($data);
        $countPage       = ceil($countData / $perPage);
        $offset          = ($currentPage - 1) * $perPage;
        $this->iteration = $countData - $offset;
        $this->result    = array_slice($data, $offset, $perPage);

        if (isset($countData, $perPage)) {
            $pages = range(1, $countPage);
            if ($pageToShow >= 1) {
                $lowestPossible = min(count($pages) - $pageToShow, intval($currentPage) - ceil($pageToShow / 2));
                $offsetPage = max(0, $lowestPossible);
                $pages = array_slice($pages, $offsetPage, $pageToShow);
            }
        }

        $nextPage     = $currentPage + 1;
        $previousPage = $currentPage - 1;

        $links = '';

        if (count($pages) > 1) {
            if ($currentPage != 1) {
                $links .= "<a class='pagination-link' href='index.php?page={$previousPage}'><div class='next-prev'>Previous</div></a>";
            }

            foreach ($pages as $page) {
                if ($currentPage == $page) {
                    $links .= "<div class='custom-pagination active'><a class='pagination-link' style='text-decoration:none; color:white;'>{$page}</a></div>";
                } else {
                    $links .= "<a class='pagination-link' href='index.php?page={$page}'><div class='custom-pagination'>{$page}</div></a>";
                }
            }
            
            if ($currentPage != $countPage) {
                $links .= "<a class='pagination-link' href='index.php?page={$nextPage}'><div class='next-prev'>Next</div></a>";
            }
        }

        return $links;
    }

    function fetchResult() 
    {
        return $this->result;
    }

    function iteration() 
    {
        return $this->iteration;
    }
}
