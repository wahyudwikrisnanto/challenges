<?php
session_start();

Class StoreValidation {

    protected $titleOld;
    protected $bodyOld;
    
    function validate($title, $body)
    {
        $titleLength = $this->titleLength($title);
        $bodyLength  = $this->bodyLength($body);
        $this->oldBody($body);
        $this->oldTitle($title);
        $this->validateTitle($titleLength);
        $this->validateBody($bodyLength);
    }

    protected function oldTitle($title)
    {
        $this->titleOld = $title;
    }

    protected function oldBody($body)
    {
        $this->bodyOld = $body;
    }

    protected function titleLength($title) 
    {
        return strlen($title);
    }

    protected function bodyLength($body) 
    {
        return strlen($body);
    }

    protected function validateTitle($titleLength)
    {
        if (($titleLength < 10 || 
            $titleLength > 32) && 
            $titleLength != 0) {
            $_SESSION['errors'][]      = ['Your title must be 10 to 32 characters long!'];
            $_SESSION['old']['title']  = [$this->titleOld];
        } elseif ($titleLength == 0) {
            $_SESSION['errors'][]      = ["Your title can't be empty!"];
            $_SESSION['old']['title']  = [$this->titleOld];
        }
    }

    protected function validateBody($bodyLength) 
    {   
        if (($bodyLength < 10  || 
            $bodyLength > 200) && 
            $bodyLength > 0) {
            $_SESSION['errors'][]     = ['Your body must be 10 to 200 characters long!'];
            $_SESSION['old']['body']  = [$this->bodyOld];
        } elseif ($bodyLength == 0) {
            $_SESSION['errors'][]     = ["Your body can't be empty!"];
            $_SESSION['old']['body']  = [$this->bodyOld];
        }
    }
}
?>