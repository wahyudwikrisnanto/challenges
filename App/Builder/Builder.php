<?php 
include 'App/Database/Connection.php';

class Builder {

    private $table;
    private $select;
    private $where;
    private $db;
    private $orderBy;

    public function __construct()
    {
        $this->db = new Connection;
    }

    public function insert(array $data)
    {
        foreach ($data as $key => $value) {
            $column[] = $key;
            $values[] = $value;
        }

        $columnString = implode(', ', $column);
        $valueString  = '"' . implode('", "', $values) . '"';

        $insert = "INSERT INTO `{$this->table}` "
                . "({$columnString}) " 
                . "VALUES ({$valueString}) ";

        $query = mysqli_prepare($this->db->connection, $insert);
        $query->execute();
    }

    public function table($table) 
    {
        $this->table = $table;
        return $this;
    }

    public function select($select) 
    {
        if (is_array($select)) {
            $array  = $select;
            $select = '';
            foreach ($array as $key => $value) {
                if ($key > 0) {
                    $select .= ", {$value}";
                } else {
                    $select .= "{$value}";
                }
            }    
        }

        $this->select = $select;

        return $this;
    }

    public function where($column, $condition, $value) 
    {
        if (isset($column, $condition, $value)) {
            $this->where = "WHERE {$column} {$condition} '{$value}'";
        } else {
            $this->where = '';
        }

        return $this;
    }

    public function orderBy($column, $type) 
    {
        if (isset($column, $type)) {
            $this->orderBy = "ORDER BY {$column} {$type}";
        } else {
            $this->orderBy = '';
        }

        return $this;
    }

    public function get() 
    {
        $query = "SELECT {$this->select} FROM `{$this->table}` " 
               . "{$this->where} " 
               . "{$this->orderBy} ";

        $data = mysqli_prepare($this->db->connection, $query);
        $data->execute();
        $fetchData = mysqli_stmt_get_result($data);

        while ($row = mysqli_fetch_assoc($fetchData)) {
            $result[] = $row;
        }

        $data->close();

        return $result;
    }

    public function first() 
    {
        $query = "SELECT {$this->select} FROM `{$this->table}` " 
               . "{$this->where} "
               . "{$this->orderBy} "
               . "LIMIT 1 ";

        $data   = mysqli_prepare($this->db->connection, $query);
        $data->execute();
        $fetchData = mysqli_stmt_get_result($data);

        $result = mysqli_fetch_assoc($fetchData);

        return $result;
    }
}

?>