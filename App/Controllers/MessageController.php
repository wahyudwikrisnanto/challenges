<?php
require_once 'App/Validations/StoreValidation.php';
require_once 'App/Pagination/Pagination.php';
require_once 'App/Builder/Builder.php';
require_once 'App/Models/MessageModel.php';

class MessageController
{
    protected $builder;

    function __construct()
    {
        $this->builder = new Builder;
    }

    function test()
    {
        $query = $this->builder->table('messages')->insert([
            'title'      => 'new',
            'body'       => 'this is body',
            'created_at' => date('Y-m-d H:i:s')
        ]);

        $model = new MessageModel;
        $model->all();
    }

    function showMessage()
    {
        $result = $this->builder
                 ->table('messages')
                 ->select('*')
                 ->orderBy('created_at', 'DESC')
                 ->get();

        if (isset($_GET['page'])) {
            $currenPage = $_GET['page'];
        } else {
            $currenPage = 1;
        }

        $pagination = new Pagination;

        if (!empty($result)) {

            $page      = $pagination->paginate($result, 10, $currenPage, 5);
            $data      = $pagination->fetchResult();
            $iteration = $pagination->iteration();
            
            $result = array(
                'page'        => $page,
                'data'        => $data,
                'currentPage' => $currenPage,
                'iteration'   => $iteration
            );

            return $result;
        }
    }

    function storeMessage($title, $body)
    {
        $validation = new StoreValidation;
        $validation->validate($title, $body);

        if (empty($_SESSION['errors'])) {
            $this->builder->table('messages')->insert([
                'title'      => $title,
                'body'       => $body,
                'created_at' => date('Y-m-d H:i:s'),
            ]);
        }
    }
}
