<?php 
require_once 'App/Controllers/MessageController.php';

$messageController = new MessageController;

$action = $_GET['action'];

if ($action == 'store') {
    $messageController->storeMessage($_POST['title'], $_POST['body']);
    header('location:index.php');
}